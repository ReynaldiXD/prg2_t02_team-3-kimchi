﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TomCafe
{
    class Item
    {
        private string name;
        private double price;
        private int quantity;

        public string Name { get => name; set => name = value; }
        public double Price { get => price; set => price = value; }
        public int Quantity { get => quantity; set => quantity = value; }

        public Item()
        {

        }

        public Item( string name, double price, int quantity)
        {
            this.name = name;
            this.price = price;
            this.quantity = quantity;
        }

        public double CalculateTotalCost()
        {
            return price * quantity;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
